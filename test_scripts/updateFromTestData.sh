#!/bin/bash
# Copy all scripts and file into the folder here.

mkdir -p CopiedFromDataFolder/
echo "These are all copyied! Don't edit here!" > ./CopiedFromDataFolder/README.txt

cp -n `find ~/as4_inst_tests | grep .sh` ./CopiedFromDataFolder/

# Tar all files in the ~/as4_inst_tests folder (without the preceeding path)
sudo tar -cvzf ./CopiedFromDataFolder/as4_test_folders.tar.gz -C ~/ as4_inst_tests
sudo chown $USER ./CopiedFromDataFolder/as4_test_folders.tar.gz
sudo chgrp $USER ./CopiedFromDataFolder/as4_test_folders.tar.gz
# NOTE: Zipped the output so it can go into git for some crazy reason.

# Extract with:
# tar -C ~/ -xzvf ./CopiedFromDataFolder/as4_test_folders.tar.gz 
