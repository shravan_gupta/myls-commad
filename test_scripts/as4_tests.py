# Full test suite for myls
# RUN THIS FILE; see README for more.

import os
import sys

# Arguments
if (len(sys.argv) != 3 and len(sys.argv) != 4):
    print("Usage:")
    print("  python3 %s <path to myls> <path to results> " % (sys.argv[0]))
    print("Examples:")
    print("  python3 %s ~/subs/g-a1 ~/as4_results/ " % (sys.argv[0]))
    print("  python3 %s ~/subs/g-a1 ~/as4_results/ > ~/as4_results/current/00-REPORT.txt" % (sys.argv[0]))
    sys.exit(0)

# Read from command line
pathToMylsRelative=sys.argv[1]
pathToMyls = os.path.abspath(sys.argv[1])
pathToResults = os.path.abspath(sys.argv[2])

totalScore = 0
totalMaxScore = 0
curTestNum = 0

# Path Constants
pathOfTestFolders = "~/as4_inst_tests/"
pathToResultsSolution = pathToResults + "/_solution"
pathToResultsCurrent = pathToResults + "/current"
pathToResultsArchives = pathToResults + "/archives"



def run_test(num, name, start_dir, args, careAboutWhitespace, useValgrind):
    global pathToMyls, pathToResults

    # Execute the test
    pathOfRunScript = pathOfTestFolders + "run_test.sh"
    outputFileName = str(num) + "-" + name + "-output.txt"
    pathToOutputFile = pathToResultsCurrent + "/" + outputFileName
    pathToSolutionFile = pathToResultsSolution + "/" + outputFileName

    # OVERRIDE HERE FOR 'ls'
    lsExec = pathToMyls + "/myls"
    # lsExec = "ls -v1"
    # print("WARNING: Using ls")

    # Verify it exists
    if not os.path.exists(lsExec):
        print("Executable under test not found; bad path? compile it?")
        print("    " + pathToMyls)
        quit()

    if useValgrind:
        valgrindOutFile = pathToOutputFile + ".valgrind"
        lsExec = "valgrind --leak-check=full --log-file=%s %s" \
            % (valgrindOutFile, lsExec)

    lsCommand = pathOfRunScript + " " \
        + "\"" + start_dir + "\" "\
        + "\"" + lsExec + " " + args + "\"" \
        + " " + pathToOutputFile
    

    # print()
    # print("-> running #" + str(num) + " " + name + ": " + lsCommand)
    sys.stdout.flush()
    os.system(lsCommand)

    # Diff the combined output file
    pathToDiffOutput = pathToOutputFile + ".diff"
    pathToDiffOutputErr = pathToOutputFile + ".differr"
    diffArgs = ""
    if (careAboutWhitespace):
        # Tight
        # - ignore whitespace change at end of line,
        # - ignore blank lines
        # - ignore lines which are only whitespace
        diffArgs = "--ignore-trailing-space --ignore-matching-lines=^\s*$ --ignore-blank-lines"
    else:    
        # Normal
        # --ignore-all-space is changes of spacing on a line
        # --ignore-blank-lines : blank lines
        # --ignore-matching-lines: whitespace or ending in : (likely a ls -R dir name)
        diffArgs = "--ignore-matching-lines=^\s*$ --ignore-matching-lines=:\s*$ --ignore-blank-lines --ignore-all-space"
    
    diffCommand = "diff %s %s %s 2> %s > %s" \
                % (diffArgs, pathToSolutionFile, pathToOutputFile, pathToDiffOutput, pathToDiffOutput)
    # print("Diff command: " + diffCommand)

    os.system(diffCommand)
    diffSize = os.path.isfile(pathToDiffOutput) and os.path.getsize(pathToDiffOutput)
    return (diffSize == 0)


def run_many_tests(name, tests):
    global totalScore, totalMaxScore, curTestNum

    print()
    print("* " + name + " *")
    score = 0
    maxScore = 0
    for testData in tests:
        curTestNum = curTestNum + 1
        name = testData[0]
        folder = testData[1]
        args = testData[2]
        points = testData[3]
        careAboutWhitespace = testData[4]
        maxScore = maxScore + points

        print(str(curTestNum) + ": " + name )
        ans = run_test(curTestNum, name, folder, args, careAboutWhitespace, False)
        if (ans == 1):
            # print(str(curTestNum) + ": Passed " + name + " (" + str(points) + " points)")
            # print("   -> 'myls " + args + "'  in  " + folder)
            score = score + points
        else:
            print("   --> FAILED " + " (" + str(points) + " points)")
            # print("   -> 'myls " + args + "'  in  " + folder)        
    print("Score: " + str(score) + " / " + str(maxScore))

    totalScore = totalScore + score
    totalMaxScore = totalMaxScore + maxScore
    
def run_manual(name, folder, args, useValgrind):
    global totalScore, totalMaxScore, curTestNum

    score = 0
    maxScore = 0

    curTestNum = curTestNum + 1

    run_test(curTestNum, name, folder, args, False, useValgrind)
    print("%s: %s -- Manually check output for test" % (str(curTestNum), name))

def generate_manual_summary():
    dir = pathToResultsCurrent + "/"
    # *.valgrind
    print()
    print("Valgrind Issues:")
    print("------------------")
    sys.stdout.flush()
    os.system('grep -Hn "definitely lost"  %s/*.valgrind' % (dir) )
    os.system('grep -Hn "still reachable"  %s/*.valgrind' % (dir) )
    os.system('grep -Hn "getgrpid"  %s/*.valgrind' % (dir) )
    os.system('grep -Hn "Invalid write of size"  %s/*.valgrind' % (dir) )
    os.system('grep -Hn "Invalid read of size"  %s/*.valgrind' % (dir) )
    print()
    print("Ensure size of 3 files > 0")
    sys.stdout.flush()
    os.system('stat -c "%s %n" ' + dir + '/*valgrind*.txt' )

    # *-bad*.txt
    print()
    print("Error on bad opt:")
    print("------------------")
    sys.stdout.flush()
    os.system("cat %s/*-bad_opt*.txt" % (dir) )
    print()
    print("Error on bad file:")
    print("------------------")
    sys.stdout.flush()
    os.system("cat %s/*-bad_file*.txt" % (dir) )

# name, folder, args, points, care about whitespace?
options_tests = [
    # -i (inodes)
    ["inode_file", "symlinks", "-i file.txt", 2, False],
    ["inode_basics", "basics", "-i", 2, False],
    ["inode_links", "symlinks", "-i", 2, False],

    # -l (long)
    ["long_basic", "basics", "-l", 2, False],
    ["long_lots", "lots_of_files", "-l", 2, False],
    ["long_symlinks", "symlinks", "-l", 2, False],
    ["long_groups_users", "groups_users", "-l", 2, False],
    ["long_permissions", "permissions", "-l", 2, False],
    ["long_dates", "dates", "-l", 2, False],

    # -R (recursion)
    ["recursion_basic", "basics", "-R", 2, False],
    ["recursion_file", "basics", "-R fileA.txt", 2, False],
    ["recursion_big", "", "-R big_recursion/", 2, False],

    # opt as filename
    ["opt_file_name", "opt_file_name", "fileA.txt -i", 1, False],

    # opt combinations
    ["opts_iR", "basics", "-iR ./", 1, False],
    ["opts_liR", "basics", "-liR", 1, False],
    ["opts_i_l", "basics", "-i -l ./", 1, False],
    ["opts_l_R", "basics", "-l -R .", 1, False],
    ["opts_i_i_l_i_l_i", "basics", "-i -i -l -i -l -i", 1, False],
]



# name, folder, args, points, care about whitespace?
file_dir_tests = [
    # Empty folder; hidden files
    ["empty", "empty", "", 2, False],
    ["hidden", "hidden", "", 2, False],

    # Multiple arguments
    ["one_file", "one_file", "", 2, False],
    ["one_file_arg", "basics", "fileA.txt", 2, False],
    ["one_dir_arg", "", "basics", 2, False],
    ["2files_2dir", "basics", "fileA.txt fileB.txt dir0 dir1", 2, False],

    # symlinks
    ["symlink", "symlinks", "soft_file.txt", 2, False],
    ["symlink_dir", "symlinks", "", 2, False],
    ["links_hard_spaces", "links_hard_space", "", 2, False],

    # big folder
    ["big_dir", "lots_of_files", "", 2, False],

    # paths [1 point each]
    ["path_dot", "basics", ".", 1, False],
    ["path_dotslash", "basics", "./", 1, False],
    ["path_dotdot", "basics", "..", 1, False],
    ["path_dotdotslash", "basics", "../", 1, False],
    ["path_absolute", "", "/home/$USER/as4_inst_tests", 1, False],
    ["path_star", "basics", "*", 1, False],
    ["path_dirstar", "", "basics/*", 1, False],
    ["path_bigstar", "lots_of_files", "*", 1, False],
    ["path_dir_dotdot_dir", "", "basics/../basics", 1, False],
    ["path_dot_dir_dotdot_dot_dir_slash", "", "./basics/.././basics/", 1, False],
    # ~ not testing because it's just expanded by the shell.
]

# name, folder, args, points, care about whitespace?
display_tests = [
    # Spacing
    ["spacing_l1",   "basics", "-l", 1, True],
    ["spacing_l2",   "", "-l basics", 1, True],
    ["spacing_R1",   "basics", "-R", 1, True],
    ["spacing_R2",   "", "-R basics", 1, True],
    ["spacing_ilR1", "basics", "-ilR", 1, True],
    ["spacing_ilR2", "", "-ilR basics", 1, True],
    ["spacing_link_spacing1", "links_hard_space", "-l", 1, True],
    ["spacing_link_spacing2", "", "-l links_hard_space", 1, True],
    ["spacing_group_user1", "groups_users", "", 1, True],
    ["spacing_group_user2", "", "groups_users", 1, True],

    # Sorting
    ["sort_files1", "sorting", "file11 file10", 2, False],
    ["sort_files2", "sorting", "file11 file10 file1", 2, False],
    ["sort_dirs", "sorting", "dir_b dir_a dir_aa", 2, False],
    ["sort_file_dir1", "sorting", "file10 file1 dir_b file11 dir_b", 2, False],
    ["sort_file_dir2", "sorting", "*", 2, False],
]


# Run the tests    
os.system("mkdir -p " + pathToResultsCurrent)
print("'myls' Test Suite")
print("-----------------")
print("Path to myls:    " + pathToMyls)
print("Path to results: " + pathToResults)

run_many_tests("Options", options_tests)
run_many_tests("Files and Directories", file_dir_tests)
run_many_tests("Display", display_tests)

# name, folder, args, valgrind?T/F
print()
print("Error Handling (manual testing)")
run_manual("valgrind_basic", "basics", "", True)
run_manual("valgrind_big_recursion", "big_recursion", "-R .", True)
run_manual("valgrind_lots", "lots_of_files", "./", True)
run_manual("bad_opt", "basics", "-e", False)
run_manual("bad_file", "basics", "ThereShouldBeNoFileOfThisName25904609943ti4", False)
generate_manual_summary()

print()
print("Final Score " + str(totalScore) + " / " + str(totalMaxScore))

# Zip up results
os.system("mkdir -p " + pathToResultsArchives)
zipName = pathToMylsRelative + ".zip"
zipName = zipName.replace("/", "_")
zipName = zipName.replace(".", "")
zipName = zipName.replace(" ", "")
zipName = zipName.replace("~", "")
pathToZip = pathToResultsArchives + "/" + zipName
print("ZIP into: " + pathToZip)
sys.stdout.flush()
os.system("zip -r -j --quiet " + pathToZip + " " + pathToResultsCurrent)