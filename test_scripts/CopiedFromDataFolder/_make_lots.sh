#!/bin/bash

# Suggested command:
# ./_make_lots.sh 5 0

levels=$1
curLevel=$2


counter=0
nextLevel=$curLevel
((nextLevel++))

echo "Making at level $curLevel / $levels;   next level is $nextLevel"

while [ $counter -lt $levels ]; do
	# Create this level
	echo "Counter $counter - Making at level $curLevel / $levels;   next level is $nextLevel" > file_$level_$counter

	((counter++))
done

if [ $curLevel -lt $levels ]; then
	
	# Recurse
	nextDir=dir_L${curLevel}
	mkdir $nextDir
	cp $0 $nextDir

	echo "at $curLevel: Run $0 $levels $nextLevel  (in folder $nextDir)"
	cd $nextDir; $0 $levels $nextLevel; cd ..
fi

echo "Done making $counter files and directories at level $curLevel / $levels;"
