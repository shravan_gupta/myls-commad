#!/bin/bash

# Usage:
#   ./run_test.sh basic/ "/some/where/myls -l ../empty" /answers/here.txt

startFolder=$1
command=$2
outputFile=$3

scriptFolder="$(dirname $0)"
stderrFile=$outputFile.stderr

# echo "## script name: " $0
# echo "## start dir:   " $startFolder
# echo "## command:     " $command
# echo "## output:      " $outputFile
# echo "## stderr:      " $stderrFile

# Cleanup previous files
rm -f $outputFile $stderrFile
cd $scriptFolder/$startFolder; $command > $outputFile 2> $stderrFile

# Combine stdout and stderr
cat $stderrFile >> $outputFile
rm $stderrFile
