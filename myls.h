/*
myls.h

Author: Shravan Gupta
Date: 24-7-20
*/

#ifndef _MYLS_H_
#define _MYLS_H_

// Print's file permissions
void printFilePermission(mode_t mode);

// Print's the username
void printUserName(uid_t uid );

// Print's the group name
void printGroupName(gid_t grpNum);

// Checks if a directory exists
int directoryCheck(char* directoryPath);

// Print's the information of file/directory
void printInfo(char* fileName, char* directoryPath, int concatFlag);

// Print's the file/directory
void printDirectory(char* directoryName);

// Sorts the array of arguments character-by-character sort according to the ASCII table
void sort(char* unsortedArgs[], int start, int end);

#endif