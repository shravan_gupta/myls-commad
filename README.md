# MyLs Commad 

Implementation of Linux ls (LiSt) command.

When calling the program, it will take the form:  **./myls [options] [file list]**

**Options Supported:**
- -i: Print the index number of each file
- -l: Use a long listing format
- -R: List subdirectories recursively

**Special Paths:** 
(x and y are arbitrary names)
- /     Root directory (when at the front of path of the path such as /usr)
- /x/y  Absolute path to a file or directory
- /xy/  Absolute path to a directory
- x/y   Relative path to a file or directory
- x/y/  Relative path to a directory
- ~     User’s home folder (can be used such as ~ or ~/test/this/out)
- .     Current directory
- ..    Up one directory (can be used such as ../ or ../../oh/my/../really/.. )  
- '*'   Wildcard (such as *.c); most of this work will be done by the shell!