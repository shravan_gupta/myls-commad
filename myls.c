/*
myls.c

Author: Shravan Gupta
Date: 24-7-20
*/

#define _XOPEN_SOURCE 700
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     // strverscmp(), strchr()
#include <time.h>       // localtime_r()

#include <unistd.h>     // struct stat, getopt
#include <sys/types.h>  // struct stat
#include <sys/stat.h>   // struct stat
#include <dirent.h>     // opendir(), readdir(), struct dirent, etc.
#include <pwd.h>        // getpwuid()
#include <grp.h>        // getgrgid()

#include "myls.h"

#define MAX_PATH_LEN 4096 

static int iFlag = 0;
static int lFlag = 0;
static int RFlag = 0; 
static int printDirFlag = 0;
// static int maxiNode = 0;
// static long int maxSize = 0;
// static char* maxUsername = NULL;
// static char* maxGroupname = NULL;

// Print's file permissions
void printFilePermission(mode_t mode) {
    // "d" = directory, "l" = link, "-" = others
    if(S_ISDIR(mode)) 
        printf("d");
    else if(S_ISLNK(mode)) 
        printf("l");
    else 
        printf("-");
    
    // User permissions
    printf((mode & S_IRUSR) ? "r" : "-");
    printf((mode & S_IWUSR) ? "w" : "-");
    printf((mode & S_IXUSR) ? "x" : "-");
    
    // Group permissions
    printf((mode & S_IRGRP) ? "r" : "-");
    printf((mode & S_IWGRP) ? "w" : "-");
    printf((mode & S_IXGRP) ? "x" : "-");
    
    // Other permissions
    printf((mode & S_IROTH) ? "r" : "-");
    printf((mode & S_IWOTH) ? "w" : "-");
    printf((mode & S_IXOTH) ? "x" : "-");
    printf(" ");
}

// Print's the username
void printUserName(uid_t uid) {
    struct passwd *pw = getpwuid(uid);

    if (pw) {
        printf("%9s ", pw->pw_name);
    } else {
        printf("No name found for %u\n", uid);
    }
}

// Print's the group name
void printGroupName(gid_t grpNum) {
    struct group *grp = getgrgid(grpNum);

    if (grp) {
        printf("%8s ", grp->gr_name);
    } else {
        printf("No group name for %u found\n", grpNum);
    }
}
/*
// Gets the max width of columns for option -i and -l
void maxWidthForColumns(char* directoryPath, int concatFlag) {
    DIR* dir;
    struct dirent *dirPtr;
    struct stat fileInfo;
    char filePath[MAX_PATH_LEN];

    // struct passwd *pw = getpwuid(fileInfo.st_uid);
    // struct group *grp = getgrgid(fileInfo.st_gid);
    // printf("%8s %ld\n", pw->pw_name, strlen(pw->pw_name));
    // printf("%8s \n", grp->gr_name);
    int iNodeDigitCount = 0;
    //int count =0;

    if((dir = opendir(directoryPath)) == NULL) {
        fprintf(stderr, "Failed to open '%s\n", directoryPath);
        exit(EXIT_FAILURE);
    }
    while ((dirPtr = readdir(dir)) != NULL) {
        if (dirPtr->d_name[0] == '.') {
            continue;
        }
        if(concatFlag){
            memset(filePath,0, sizeof(filePath));
            snprintf(filePath, sizeof(filePath), "%s/%s", directoryPath, dirPtr->d_name);
        }
        else{
            strcpy(filePath, dirPtr->d_name);
        }
        if(lstat(filePath, &fileInfo) < 0) {
            fprintf(stderr, "%s: No such directory or file doesn't exist", filePath);
            exit(EXIT_FAILURE);
        }
        char iNodeString[50];
        //sprintf(iNodeString, "%ld", theStat.st_ino);
        iNodeDigitCount = 0;
        for(int i = 0; i < strlen(iNodeString); i++) {
                iNodeDigitCount++;
        }
        if(iNodeDigitCount > maxiNode) {
            maxiNode = iNodeDigitCount;
        }

        // if(fileInfo.st_ino > maxiNode) {
        //     maxiNode = fileInfo.st_ino;
        // }
        // if(fileInfo.st_size > maxSize) {
        //     maxSize = fileInfo.st_size;
        // }
        // if((strlen(pw->pw_name)) > (strlen(maxUsername))) {
        //     maxUsername = pw->pw_name;
        // }
        // if((strlen(grp->gr_name)) > (strlen(maxGroupname))) {
        //     maxGroupname = grp->gr_name;
        // }
    }
    //printf("maxInode: %d\n", maxiNode);
    // printf("maxUsr: %s\n", maxUsername);
    // printf("maxGrp: %s\n", maxGroupname);
    // printf("maxSize: %ld\n", maxSize);
    //closedir(dir);
}
*/

// Checks if a directory exists
// 1 if directory exists, 0 if directory doeesn't exist but file does, -1 if file doesn't exist
int directoryCheck(char* directoryPath) {
    struct stat fileInfo;
    if(lstat(directoryPath, &fileInfo) < 0) {
        return -1;
    }
    return S_ISDIR(fileInfo.st_mode);
}

// Print's the information of file/directory
void printInfo(char* fileName, char* directoryPath, int concatFlag) {
    char filePath[MAX_PATH_LEN];
    char dataStr[20];
    struct stat fileInfo;
    struct tm mtime;

    if(concatFlag){
        memset(filePath,0, sizeof(filePath));
        snprintf(filePath, sizeof(filePath), "%s/%s", directoryPath, fileName);
    }
    else{
        strcpy(filePath, fileName);
    }
    if(lstat(filePath, &fileInfo) < 0) {
        fprintf(stderr, "%s: No such directory or file doesn't exist", filePath);
        exit(EXIT_FAILURE);
    }
    // iFlag
    if(iFlag) {
        printf("%18lu ", fileInfo.st_ino);
    }
    //lFlag
    if(lFlag) {
        printFilePermission(fileInfo.st_mode);
        printf("%2lu", fileInfo.st_nlink);
        printUserName(fileInfo.st_uid);
        printGroupName(fileInfo.st_gid);
        printf("%8ld ", fileInfo.st_size);
        localtime_r(&(fileInfo.st_mtime), &mtime);
        strftime(dataStr, 20, "%b %d %Y %H:%M ", &mtime);
        printf("%s", dataStr);
    }
    
    // Compare file name with special chars- space, !, $, ^, &, (, ) 
    if(strchr(fileName,' ') != NULL || strchr(fileName,'!') != NULL || strchr(fileName,'$') != NULL || strchr(fileName,'^') != NULL 
        || strchr(fileName,'&') != NULL || strchr(fileName,'(') != NULL || strchr(fileName,')') != NULL)
        printf("'%s'", fileName);
    else if(lFlag)
        printf(" %s", fileName);
    else
        printf("%s", fileName); 
	
    // Handle symbolic link
    if(lFlag){
        if(S_ISLNK(fileInfo.st_mode)) {
            char targetPath[MAX_PATH_LEN];
            memset(targetPath, 0, sizeof(targetPath));

            if(readlink(filePath, targetPath, sizeof(targetPath) - 1) < 0) {
                fprintf(stderr, "Failed to get target path");
                exit(EXIT_FAILURE);
            }
            if(strchr(targetPath,' ') != NULL || strchr(targetPath,'!') != NULL || strchr(targetPath,'$') != NULL || strchr(targetPath,'^') != NULL 
                || strchr(targetPath,'&') != NULL || strchr(targetPath,'(') != NULL || strchr(targetPath,')') != NULL)
                printf(" -> '%s'", targetPath);
            else
                printf(" -> %s", targetPath);

        }
    }
    printf("\n");
}

// Print's the file/directory
void printDirectory(char* directoryPath) {

    // Check for invalid directory path or file doesn't exist
    struct stat fileInfo;
    if(lstat(directoryPath, &fileInfo) < 0 || directoryPath == NULL) {
        fprintf(stderr, "%s: No such directory or file exist\n", directoryPath);
        exit(EXIT_FAILURE);
    }
    // When directory exists
    else if(S_ISDIR(fileInfo.st_mode)) {
        DIR* dir;
        struct dirent** nameList;
        int fileCount;
        int iter = 0;

        // Read and sort the file/directory 
        fileCount = scandir(directoryPath, &nameList, NULL, versionsort);
        if(fileCount == -1) {
            perror("scandir");
            exit(EXIT_FAILURE);
        }
        if((dir = opendir(directoryPath)) == NULL) {
            fprintf(stderr, "Failed to open '%s\n", directoryPath);
            exit(EXIT_FAILURE);
        }
        if(dir != NULL) {
            printf("\n");
            if(RFlag || printDirFlag) 
                printf("%s: \n", directoryPath);

            // Update column width
            //maxiNode = 0;
            //maxSize = 0;
            //maxWidthForColumns(directoryPath, 1);
                
            // First pass: Print the name 
            while(iter < fileCount) {
                // Skip hidden '.' files
                if(nameList[iter]->d_name[0] == 46) {
                    if(!RFlag)
                        free(nameList[iter]);
                    iter++;
                    continue;
                }
                printInfo(nameList[iter]->d_name, directoryPath, 1);
                if(!RFlag)
                    free(nameList[iter]);
                iter++;
            }
            // Second pass: Print the information
            // RFlag
            if(RFlag) {
                iter = 0;
                rewinddir(dir);
                while(iter < fileCount) {
                    // Skip hidden '.' files
                    if(nameList[iter]->d_name[0] == 46) {
						free(nameList[iter]);
                        iter++;
                        continue;
                    }
                    char filePath[MAX_PATH_LEN];
                    memset(filePath,0, sizeof(filePath));
                    snprintf(filePath, sizeof(filePath), "%s/%s", directoryPath, nameList[iter]->d_name);

                    if(lstat(filePath, &fileInfo) < 0) {
                        fprintf(stderr, "%s: No such directory or file exist\n", filePath);
                        exit(EXIT_FAILURE);
                    }
                    if(S_ISDIR(fileInfo.st_mode)) {
                        printDirFlag = 1;
                        printDirectory(filePath);
                    }
                    free(nameList[iter]);
                    iter++;
                }
            }
        }
        free(nameList);
        closedir(dir);
    }
    // When directory doeesn't exist but file does
    else {
        printInfo(directoryPath, "", 0);
    }
}

// Sorts the array of arguments character-by-character sort according to the ASCII table
void sort(char* unsortedArgs[], int start, int end) {
    char* tempArgs = NULL;
    char* minArgs = NULL;
    int minIndex;
    for(int i = start; i < end-1; i++) {
        minIndex = i;
        minArgs = unsortedArgs[i];
        for(int j = i+1; j < end; j++) {
            if(strverscmp(minArgs, unsortedArgs[j]) > 0) {
                minArgs = unsortedArgs[j];
                minIndex = j;   
            }    
        }
        tempArgs = unsortedArgs[i];
        unsortedArgs[i] = unsortedArgs[minIndex];
        unsortedArgs[minIndex] = tempArgs;
    }
}

int main(int argc, char* argv[]) {
    char* unsortedArgs[argc-1];
    char* sortedArgs[argc-1];
    int argsCount = 0;
    int iter = 0;
    int start = 0;
    int end = 0;

    // Used getopt() function to parse the command-line arguments
    // If the first character of optstring is '+', then option processing 
    // stops as soon as a nonoption argument is encountered
    int flagPos;
    while((flagPos = getopt(argc, argv, "+ilR")) != -1) {
        switch(flagPos) {
            case 'i':
                iFlag = 1;
                break;
            case 'l':
                lFlag = 1;
                break;
            case 'R':
                RFlag = 1;
                break;
            default:
                fprintf(stderr, "Usage: ./myls [-ilR] [file list...]\n");
                return EXIT_FAILURE;
            }
    }

    // Copy the arguments in an array, excluding the options
    for(int i = 0; i < argc-1; i++) {
        if(argv[i+1][0] == '-') {
            if(strchr(argv[i+1], 'i') != NULL || strchr(argv[i+1], 'l') != NULL || strchr(argv[i+1], 'R') != NULL)
                continue;
        }
        unsortedArgs[iter] = argv[i+1];
        iter++;
    }
    argsCount = iter;
    iter = 0;
    // Move all the files before folders
    for(int i = 0; i < argsCount; i++) {
        // 0 if directory doeesn't exist but file does
        if(directoryCheck(unsortedArgs[i]) == 0) {
            sortedArgs[iter] = unsortedArgs[i];
            iter++;
        }
    }
    start = iter;
    end = iter;
    // Move all the folders after files
    for(int i = 0; i < argsCount; i++) {
        // 1 if directory exists
        if(directoryCheck(unsortedArgs[i]) == 1) {
            sortedArgs[iter] = unsortedArgs[i];
            iter++;
        }
    }
    // Sort the files
    sort(sortedArgs, 0, end);
    // Sort the folders
    sort(sortedArgs, start, argsCount);

    // If no file or directory list is given, print current directory
    int index = 0;
    if(argsCount == 0) {
        printDirectory(".");
    }
    // If only one file path is given
    if(argsCount == 1) {
        printDirectory(sortedArgs[argsCount-1]);
        index++;
    }
    //If more than one file path is given
    while(index < argsCount) {
        printDirFlag = 1;
        printDirectory(sortedArgs[index]);
        index++;
    }
    
    return 0;
}