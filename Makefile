
CFLAGS = -Werror -Wall -g -std=c11

run: myls.o
	gcc $(CFLAGS) -o myls myls.o
 
myls.o: myls.c myls.h
	gcc $(CFLAGS) -c myls.c

clean:
	rm *.o myls
